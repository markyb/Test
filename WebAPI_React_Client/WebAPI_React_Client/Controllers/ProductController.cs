﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_React_Client.Models;
using System.Web.Http.Cors;

namespace WebAPI_React_Client.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ProductController : ApiController
    {
        private CountryController countryController;

        public ProductController()
        {
            countryController = new CountryController();
        }

        public List<Product> GetAll(int countryId)
        {
            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == countryId);
            var products = GetAll();
            foreach (var item in products) 
            {
                item.Price = Math.Round(item.Price * country.CurrencyConversionRate, 2);
            }

            return products;
        }

        private List<Product> GetAll()
        {
            //prices in AUD
            var productList = new List<Product>
            {
                new Product{Id=101,Name="Pen",Price=5},

                new Product{Id=102,Name="Paper",Price=10},

                new Product{Id=103,Name="Mouse",Price=20},

                new Product{Id=104,Name="Keyboard",Price=30},

                new Product{Id=105,Name="Headphone",Price=40}
            };
            return productList;
        }
    }
}
