﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_React_Client.Models;
using System.Web.Http.Cors;

namespace WebAPI_React_Client.Controllers
{
    [EnableCors("*", "*", "*")]
    public class OrderController : ApiController
    {
        private ProductController productController;
        private CountryController countryController;
        private const int smallDeliveryMaxTotal = 50;
        private const int smallDelivery = 10;
        private const int bigDelivery = 20;

        public OrderController() 
        {
            productController = new ProductController();
            countryController = new CountryController();
        }

        public decimal CalculateShippingCost(List<OrderProduct> orderProducts, int countryId)
        {
            decimal total = 0;
            var products = productController.GetAll((int)CountryEnum.Australia);

            foreach (var orderItem in orderProducts)
            {
                total += orderItem.Quantity * products.First(x=> x.Id == orderItem.ProductId).Price;
            }

            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == countryId);

            var shippingCost = (total <= smallDeliveryMaxTotal ? smallDelivery : bigDelivery) * country.CurrencyConversionRate;

            return Math.Round(shippingCost,2);
        }

        public decimal CalculateBasketTotal(List<OrderProduct> orderProducts, int countryId)
        {
            decimal productsTotal = 0;
            var products = productController.GetAll(countryId);

            foreach (var orderItem in orderProducts)
            {
                productsTotal += orderItem.Quantity * products.First(x => x.Id == orderItem.ProductId).Price;
            }

            return Math.Round(productsTotal, 2) + CalculateShippingCost(orderProducts, countryId);
        }

        public void PlaceOrder(List<OrderProduct> orderProducts, int countryId)
        {
            //return success to the page.Once that is complete the page will navigate to a 'thank you' page.
        }
    }
}
