﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_React_Client.Models;
using System.Web.Http.Cors;

namespace WebAPI_React_Client.Controllers
{
    [EnableCors("*", "*", "*")]
    public class CountryController : ApiController
    {
        public List<Country> GetCurrencyConversioRates()
        {
            var countryList = new List<Country>
            {
                new Country{Id=101,Name="Australia",CurrencyConversionRate=1m},
                new Country{Id=102,Name="USA",CurrencyConversionRate=0.71m},
                new Country{Id=103,Name="NZ",CurrencyConversionRate=1.10m}
            };
            return countryList;
        }
    }
}
