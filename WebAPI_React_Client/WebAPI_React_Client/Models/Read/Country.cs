﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI_React_Client.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal CurrencyConversionRate { get; set; }
    }
}