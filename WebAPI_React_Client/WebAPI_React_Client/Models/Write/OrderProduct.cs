﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI_React_Client.Models
{
    public class OrderProduct
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}