﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using WebAPI_React_Client.Controllers;
using WebAPI_React_Client.Models;

namespace UnitTest_WebAPI_React_Client
{
    [TestClass]
    public class UnitTestProduct
    {
        [TestMethod]
        public void GetAllProducts_ShouldReturnAll()
        {
            var controller = new ProductController();

            var result = controller.GetAll((int)CountryEnum.Australia);
            Assert.AreEqual(5, result.Count);
        }

        [TestMethod]
        public void GetAllProducts_ShouldContainPen()
        {
            var controller = new ProductController();

            var result = controller.GetAll((int)CountryEnum.Australia);
            Assert.AreEqual(true, result.Any(x => x.Name == "Pen"));
        }

        [TestMethod]
        public void GetAllProducts_ConfirmPricePenAustralia()
        {
            var controller = new ProductController();

            var result = controller.GetAll((int)CountryEnum.Australia);
            Assert.AreEqual(5, result.First(x => x.Name == "Pen").Price);
        }

        [TestMethod]
        public void GetAllProducts_ConfirmPricePenUSA()
        {
            var productController = new ProductController();
            var countryController = new CountryController();

            var countryId = (int)CountryEnum.USA;
            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == countryId);

            var resultAustralia = productController.GetAll((int)CountryEnum.Australia);
            var expectedPrice = Math.Round(resultAustralia.First(x => x.Name == "Pen").Price * country.CurrencyConversionRate, 2);

            var resultUSA = productController.GetAll(countryId);
            Assert.AreEqual(expectedPrice, resultUSA.First(x => x.Name == "Pen").Price);
        }

        [TestMethod]
        public void GetAllProducts_ConfirmPricePenNZ()
        {
            var productController = new ProductController();
            var countryController = new CountryController();

            var countryId = (int)CountryEnum.NZ;
            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == countryId);

            var resultAustralia = productController.GetAll((int)CountryEnum.Australia);
            var expectedPrice = Math.Round(resultAustralia.First(x => x.Name == "Pen").Price * country.CurrencyConversionRate, 2);

            var resultUSA = productController.GetAll(countryId);
            Assert.AreEqual(expectedPrice, resultUSA.First(x => x.Name == "Pen").Price);
        }
    }
}
