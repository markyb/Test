﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WebAPI_React_Client.Controllers;
using WebAPI_React_Client.Models;

namespace UnitTest_WebAPI_React_Client
{
    [TestClass]
    public class UnitTestOrder
    {
        [TestMethod]
        public void Order_SmallShippingCost()
        {
            var expectedShipingCost = 10m;
            var oderController = new OrderController();
            var countryController = new CountryController();

            var order = new List<OrderProduct>();
            order.Add(new OrderProduct { ProductId = 105, Quantity = 1 });
            order.Add(new OrderProduct { ProductId = 102, Quantity = 1 });

            var result = oderController.CalculateShippingCost(order, (int)CountryEnum.Australia);
            Assert.AreEqual(expectedShipingCost, result);

            result = oderController.CalculateShippingCost(order, (int)CountryEnum.USA);
            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == (int)CountryEnum.USA);
            Assert.AreEqual(Math.Round(expectedShipingCost * country.CurrencyConversionRate, 2), result);

            result = oderController.CalculateShippingCost(order, (int)CountryEnum.NZ);
            country = countryController.GetCurrencyConversioRates().First(x => x.Id == (int)CountryEnum.NZ);
            Assert.AreEqual(Math.Round(expectedShipingCost * country.CurrencyConversionRate, 2), result);
        }

        [TestMethod]
        public void Order_BigShippingCost()
        {
            var expectedShipingCost = 20m;
            var oderController = new OrderController();
            var countryController = new CountryController();

            var order = new List<OrderProduct>();
            order.Add(new OrderProduct { ProductId = 105, Quantity = 1 });
            order.Add(new OrderProduct { ProductId = 102, Quantity = 1 });
            order.Add(new OrderProduct { ProductId = 101, Quantity = 1 });

            var result = oderController.CalculateShippingCost(order, (int)CountryEnum.Australia);
            Assert.AreEqual(expectedShipingCost, result);

            result = oderController.CalculateShippingCost(order, (int)CountryEnum.USA);
            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == (int)CountryEnum.USA);
            Assert.AreEqual(Math.Round(expectedShipingCost * country.CurrencyConversionRate, 2), result);

            result = oderController.CalculateShippingCost(order, (int)CountryEnum.NZ);
            country = countryController.GetCurrencyConversioRates().First(x => x.Id == (int)CountryEnum.NZ);
            Assert.AreEqual(Math.Round(expectedShipingCost * country.CurrencyConversionRate, 2), result);
        }

        [TestMethod]
        public void OrderTotal_SmallShippingCost_Australia()
        {
            var oderController = new OrderController();

            var order = new List<OrderProduct>();
            order.Add(new OrderProduct { ProductId = 103, Quantity = 2 });

            var result = oderController.CalculateBasketTotal(order, (int)CountryEnum.Australia);

            var total = (20 * 2) + 10;

            Assert.AreEqual(total, result);
        }

        [TestMethod]
        public void OrderTotal_BigShippingCost_USA()
        {
            var oderController = new OrderController();
            var countryController = new CountryController();

            var order = new List<OrderProduct>();
            order.Add(new OrderProduct { ProductId = 104, Quantity = 2 });

            var result = oderController.CalculateBasketTotal(order, (int)CountryEnum.USA);

            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == (int)CountryEnum.USA);

            var total = ((Math.Round(30 * country.CurrencyConversionRate, 2)) * 2) + Math.Round(20 * country.CurrencyConversionRate, 2);

            Assert.AreEqual(total, result);
        }

        [TestMethod]
        public void OrderTotal_SmallShippingCost_NZ()
        {
            var oderController = new OrderController();
            var countryController = new CountryController();

            var order = new List<OrderProduct>();
            order.Add(new OrderProduct { ProductId = 103, Quantity = 2 });

            var result = oderController.CalculateBasketTotal(order, (int)CountryEnum.NZ);

            var country = countryController.GetCurrencyConversioRates().First(x => x.Id == (int)CountryEnum.NZ);

            var total = ((Math.Round(20 * country.CurrencyConversionRate, 2)) * 2) + Math.Round(10 * country.CurrencyConversionRate, 2);

            Assert.AreEqual(total, result);
        }
    }
}
