﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using WebAPI_React_Client.Controllers;

namespace UnitTest_WebAPI_React_Client
{
    [TestClass]
    public class UnitTestCountry
    {
        [TestMethod]
        public void GetAllCountryCurrencyConversioRates_ShouldReturnAll()
        {
            var controller = new CountryController();

            var result = controller.GetCurrencyConversioRates();
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod]
        public void GetAllCountryCurrencyConversioRates_ShouldContainAustralia()
        {
            var controller = new CountryController();

            var result = controller.GetCurrencyConversioRates();
            Assert.AreEqual(true, result.Any(x => x.Name == "Australia"));
        }
    }
}
